package lecture09

import cats.Monad
import cats.effect.IO

import scala.io.StdIn.readLine
import scala.language.higherKinds
import scala.util.Try

object App4 {
  trait Console[F[_]] {
    def getStringLine: F[String]

    def putStringLine(line: String): F[Unit]
  }

  object Console {
    def apply[F[_] : Console]: Console[F] = implicitly[Console[F]]

    def putStringLine[F[_] : Console](line: String): F[Unit] =
      Console[F].putStringLine(line)

    def getStringLine[F[_] : Console]: F[String] =
      Console[F].getStringLine
  }

  trait Random[F[_]] {
    def nextInt(upper: Int): F[Int]
  }

  object Random {
    def apply[F[_] : Random]: Random[F] = implicitly[Random[F]]

    def nextInt[F[_] : Random](upper: Int): F[Int] =
      Random[F].nextInt(upper)
  }

  class Game[F[_]: Monad : Random : Console] {
    def parseInt(s: String): Option[Int] =
      Try(s.toInt).toOption

    import cats.syntax.all._ // импортировать синтаксис всех cats-тайпклассов
    // import cats.instances.option._ // импортировать инстансы cats-тайпклассов для Option
    import Console._
    import Random._

    def gameLoop(name: String): F[Unit] =
      for {
        num <- nextInt[F](5).map(_ + 1)
        _ <- putStringLine(s"Dear $name, please guess a number from 1 to 5:")
        input <- getStringLine
        _ <-
          parseInt(input).fold(
            putStringLine("You did not enter a number")
          ) { guess =>
            if (guess == num) {
              putStringLine(s"You guessed right, $name!")
            } else {
              putStringLine(s"You guessed wrong, $name! The number was: $num")
            }
          }
        cont <- checkContinue(name)
        _ <- if (cont) gameLoop(name) else ().pure
      } yield ()

    def checkContinue(name: String): F[Boolean] =
      for {
        _ <- putStringLine(s"Do you want to continue, $name?")
        input <- getStringLine
        result <-
          input match {
            case "y" => true.pure
            case "n" => false.pure
            case _ => checkContinue(name)
          }
      } yield result

    def main: F[Unit] =
      for {
        _ <- putStringLine[F]("What is your name?")
        name <- getStringLine[F]
        _ <- putStringLine[F](s"Hello, $name, welcome to the game!")
        _ <- gameLoop(name)
      } yield ()
  }

  def mainIO: IO[Unit] = {
    implicit val consoleIO = new Console[IO] {
      override def getStringLine: IO[String] =
        IO(readLine())

      override def putStringLine(line: String): IO[Unit] =
        IO(println(line))
    }

    implicit val randomIO = new Random[IO] {
      override def nextInt(upper: Int): IO[Int] =
        IO(scala.util.Random.nextInt(upper))
    }

    new Game[IO].main
  }


  def main(args: Array[String]): Unit =
    mainIO.unsafeRunSync()
}
