package lecture09

import cats.Applicative
import cats.data.{IndexedStateT, StateT}
import cats.effect.IO

import scala.io.StdIn.readLine
import scala.language.higherKinds

object App5 {
  import App4._

  final case class Seed(long: Long) {
    def next = Seed(long * 6364136223846793005L + 1442695040888963407L)
  }

  type Task[A] = StateT[IO, Seed, A]

  implicit val consoleTask = new Console[Task] {
    override def getStringLine: Task[String] =
      IndexedStateT.liftF(IO(readLine()))

    override def putStringLine(line: String): Task[Unit] =
      IndexedStateT.liftF(IO(println(line)))
  }

//  implicit val randomTask = new Random[Task] {
//    override def nextInt(upper: Int): Task[Int] =
//      StateT[IO, Seed, Int] { seed =>
//
//        IO.pure((seed.next, seed.long.toInt.abs % upper))
//      }
//  }

  implicit def randomState[F[_]: Applicative] = new Random[StateT[F, Seed, *]] {
    override def nextInt(upper: Int): StateT[F, Seed, Int] =
      StateT[F, Seed, Int] { seed =>

        Applicative[F].pure((seed.next, seed.long.toInt.abs % upper))
      }
  }

  def mainTask: Task[Unit] =
    new Game[Task].main


  def main(args: Array[String]): Unit =
    mainTask.run(Seed(13)).unsafeRunSync()
}
