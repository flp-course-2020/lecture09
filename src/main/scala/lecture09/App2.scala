package lecture09

import scala.io.StdIn.readLine
import scala.util.Try

object App2 {

  def parseInt(s: String): Option[Int] =
    Try(s.toInt).toOption

  case class MIO[A](unsafeRun: () => A) {
    self =>

    def map[B](f: A => B): MIO[B] =
      MIO(() => f(self.unsafeRun()))

    def flatMap[B](f: A => MIO[B]): MIO[B] =
      MIO(() => f(self.unsafeRun()).unsafeRun())
  }

  object MIO {
    def pure[A](a: => A): MIO[A] = MIO(() => a)
  }

  def putStringLine(line: String): MIO[Unit] = MIO(() => println(line))

  def getStringLine: MIO[String] = MIO(() => readLine())

  def nextInt(upper: Int): MIO[Int] =
    MIO(() => scala.util.Random.nextInt(upper))

  def gameLoop(name: String): MIO[Unit] =
    for {
      num <- nextInt(5).map(_ + 1)
      _ <- putStringLine(s"Dear $name, please guess a number from 1 to 5:")
      input <- getStringLine
      _ <-
         parseInt(input).fold(
           putStringLine("You did not enter a number")
         ) { guess =>
           if (guess == num) {
             putStringLine(s"You guessed right, $name!")
           } else {
             putStringLine(s"You guessed wrong, $name! The number was: $num")
           }
         }
      cont <- checkContinue(name)
      _ <- if (cont) gameLoop(name) else MIO.pure(())
    } yield ()

  def checkContinue(name: String): MIO[Boolean] =
    for {
      _ <- putStringLine(s"Do you want to continue, $name?")
      input <- getStringLine
      result <-
        input match {
          case "y" => MIO.pure(true)
          case "n" => MIO.pure(false)
          case _ => checkContinue(name)
        }
    } yield result

  def mainMIO: MIO[Unit] =
    for {
      _ <- putStringLine("What is your name?")
      name <- getStringLine
      _ <- putStringLine(s"Hello, $name, welcome to the game!")
      _ <- gameLoop(name)
    } yield ()

  def main(args: Array[String]): Unit =
    mainMIO.unsafeRun()
}
