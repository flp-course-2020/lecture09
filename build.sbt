name := "lecture09"

version := "0.1"

scalaVersion := "2.12.10"

libraryDependencies += "org.typelevel" %% "cats-effect" % "2.1.3"

addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full)
